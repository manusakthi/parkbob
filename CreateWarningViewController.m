//
//  CreateWarningViewController.m
//  ParkBob
//
//  Created by Manu on 11/12/15.
//  Copyright © 2015 Perfomatix. All rights reserved.
//

#import "CreateWarningViewController.h"
#import "SWRevealViewController.h"
@interface CreateWarningViewController ()
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sideMenuBar;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPickerView *projectPickerView;
@property(nonatomic,strong)NSArray *projectList;
@property (strong, nonatomic) IBOutlet UIImageView *attachmentImageView;
@end

@implementation CreateWarningViewController

- (void)viewDidLoad {
    [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, 590)];
    self.projectList=[[NSArray alloc]initWithObjects:@"Project Name 1",@"Project Name 2",@"Project Name 3",@"Project Name 4",@"Project Name 5",@"Project Name 6", nil];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"img_bg.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    self.title=@"Create Warning";
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.scrollView setContentOffset:CGPointMake(0,0)];
    self.navigationController.navigationBarHidden=NO;
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        //revealViewController.delegate=self;
        [self.sideMenuBar setTarget: self.revealViewController];
        [self.sideMenuBar setAction: @selector( revealToggle: )];
        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }

}
#pragma mark - Picker Delegates
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.projectList.count;
}
- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.projectList[row];
}
- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)attachPhoto:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Select"
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet]; // 1
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Take Photo"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                             [self openCamera];
                                                          }]; // 2
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Select Image"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               [self openGallery];
                                                           }];
    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               
                                                           }];
    
    [alert addAction:firstAction]; // 4
    [alert addAction:secondAction]; // 5
    [alert addAction:thirdAction];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)openCamera
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate=self;
        imagePickerController.allowsEditing=YES;
        imagePickerController.view.tag = 102;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePickerController animated:YES completion:^{
            
        }];
    }
    else
    {
        UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"CAM_NOT_AVAILABLE", nil)delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alt show];
    }

}
-(void)openGallery
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing=YES;
    imagePickerController.view.tag = 102;
    
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePickerController animated:YES completion:^{
    }];
    
}

#pragma mark -
#pragma mark - UIImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.attachmentImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.attachmentImageView.clipsToBounds = YES;
    self.attachmentImageView.image=[info objectForKey:UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}



@end
