//
//  WarningListViewController.h
//  ParkBob
//
//  Created by Manu on 11/12/15.
//  Copyright © 2015 Perfomatix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WarningListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@end
