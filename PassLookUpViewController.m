//
//  PassLookUpViewController.m
//  ParkBob
//
//  Created by Manu on 10/12/15.
//  Copyright © 2015 Perfomatix. All rights reserved.
//

#import "PassLookUpViewController.h"
#import "SWRevealViewController.h"

@interface PassLookUpViewController ()
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sideBarMenu;
@property (strong, nonatomic) IBOutlet UIPickerView *projectPickerView;
@property(nonatomic,strong)NSArray *projectList;
@end

@implementation PassLookUpViewController

- (void)viewDidLoad {
   
       self.projectList=[[NSArray alloc]initWithObjects:@"Project Name 1",@"Project Name 2",@"Project Name 3",@"Project Name 4",@"Project Name 5",@"Project Name 6", nil];
    [super viewDidLoad];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"img_bg.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
     self.title=@"Pass LookUp";
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
      self.navigationController.navigationBarHidden=NO;
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        //revealViewController.delegate=self;
        [self.sideBarMenu setTarget: self.revealViewController];
        [self.sideBarMenu setAction: @selector( revealToggle: )];
        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Picker Delegates
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.projectList.count;
}
- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.projectList[row];
}
- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
@end
