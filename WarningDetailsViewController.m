//
//  WarningDetailsViewController.m
//  ParkBob
//
//  Created by Manu on 11/12/15.
//  Copyright © 2015 Perfomatix. All rights reserved.
//

#import "WarningDetailsViewController.h"

@interface WarningDetailsViewController ()

@end

@implementation WarningDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"Warning List";
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"img_bg.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
